package com.wakeel.weatheronimage.history

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.wakeel.weatheronimage.R
import com.wakeel.weatheronimage.data.model.History
import com.wakeel.weatheronimage.utils.ImageGetter
import java.io.File


class HistoryAdapter(history: List<History>) :
    RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {
    private var historyList: List<History>? = null

    init {
        historyList = history
    }

    class HistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val historyImg: ImageView = itemView.findViewById(R.id.history_img)
        val imageNameTxt: TextView = itemView.findViewById(R.id.image_name_txt)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.history_item, parent, false)
        val historyHolder = HistoryViewHolder(view)
        historyHolder.historyImg.setOnClickListener {

        }
        return historyHolder
    }

    override fun getItemCount(): Int {
        return historyList?.size ?: 0
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        val history: History = historyList!![position]
        holder.historyImg.tag = history.path

        val task = ImageGetter(holder.historyImg)
        task.execute(File(holder.historyImg.tag.toString()))
        holder.historyImg.tag = task
        holder.imageNameTxt.text = history.name
    }
}
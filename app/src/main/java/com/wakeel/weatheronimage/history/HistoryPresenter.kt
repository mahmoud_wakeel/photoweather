package com.wakeel.weatheronimage.history

import com.wakeel.weatheronimage.data.model.History
import com.wakeel.weatheronimage.data.repository.HistoryRepository

class HistoryPresenter(private val historyRepository: HistoryRepository) :
    HistoryContract.Presenter {
    override fun getHistory(): List<History> {
        return historyRepository.getAllImages()!!
    }

}
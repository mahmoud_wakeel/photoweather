package com.wakeel.weatheronimage.history

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.wakeel.weatheronimage.R
import com.wakeel.weatheronimage.data.model.History
import com.wakeel.weatheronimage.data.repository.HistoryRepository
import kotlinx.android.synthetic.main.activity_history.*
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class HistoryActivity : AppCompatActivity() {
    private var presenter: HistoryPresenter? = null
    private var adapter: HistoryAdapter? = null
    private var images: List<History>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        presenter = HistoryPresenter(HistoryRepository(this))
        val layoutManager = GridLayoutManager(this, 3)
        history_list.layoutManager = layoutManager

        Observable.fromCallable {
            presenter!!.getHistory()
        }.doOnNext { list ->
            images = list
        }.doOnCompleted {
            adapter = HistoryAdapter(images!!)
            history_list.adapter = adapter
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }
}
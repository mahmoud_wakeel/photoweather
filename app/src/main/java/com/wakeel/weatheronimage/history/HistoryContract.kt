package com.wakeel.weatheronimage.history

import com.wakeel.weatheronimage.data.model.History

interface HistoryContract {
    interface Presenter {
        fun getHistory(): List<History>
    }
}
package com.wakeel.weatheronimage.utils;


public interface OnOperationResult {
    void onOperationResult(Image pojo);
}

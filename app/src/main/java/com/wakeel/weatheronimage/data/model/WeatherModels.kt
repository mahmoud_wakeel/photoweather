package com.wakeel.weatheronimage.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Clouds(
    @SerializedName("all") @Expose
    var all: Int? = null
)

data class Coord(
    @SerializedName("lon") @Expose
    var lon: Double? = null,
    @SerializedName("lat")
    @Expose
    var lat: Double? = null
)

data class Main(
    @SerializedName("temp") @Expose
    var temp: Double = 0.0,
    @SerializedName("pressure")
    @Expose
    val pressure: Int? = null,
    @SerializedName("humidity")
    @Expose
    val humidity: Int? = null,
    @SerializedName("temp_min")
    @Expose
    val tempMin: Double? = null,
    @SerializedName("temp_max")
    @Expose
    var tempMax: Double? = null
)

data class Sys(
    @SerializedName("type") @Expose
    var type: Int? = null,
    @SerializedName("id")
    @Expose
    val id: Int? = null,
    @SerializedName("message")
    @Expose
    val message: Double? = null,
    @SerializedName("country")
    @Expose val country: String? = null,
    @SerializedName("sunrise")
    @Expose
    val sunrise: Int? = null,
    @SerializedName("sunset")
    @Expose
    val sunset: Int? = null
)

data class Weather(
    @SerializedName("id") @Expose
    var id: Int? = null,
    @SerializedName("main")
    @Expose
    val main: String? = null,
    @SerializedName("description")
    @Expose
    val description: String? = null,
    @SerializedName("icon")
    @Expose
    var icon: String? = null
)

data class WeatherResponse(
    @SerializedName("coord") @Expose
    var coord: Coord? = null,
    @SerializedName("weather")
    @Expose
    val weather: List<Weather>? = null,
    @SerializedName("base")
    @Expose
    val base: String? = null,
    @SerializedName("main")
    @Expose
    val main: Main? = null,
    @SerializedName("visibility")
    @Expose
    val visibility: Int? = null,
    @SerializedName("wind")
    @Expose
    val wind: Wind? = null,
    @SerializedName("clouds")
    @Expose
    val clouds: Clouds? = null,
    @SerializedName("dt")
    @Expose
    val dt: Int? = null,
    @SerializedName("sys")
    @Expose val sys: Sys? = null,
    @SerializedName("id")
    @Expose
    val id: Int? = null,
    @SerializedName("name")
    @Expose
    val name: String? = null,
    @SerializedName("cod")
    @Expose
    var cod: Int? = null
)

data class Wind(
    @SerializedName("speed") @Expose
    var speed: Double? = null,
    @SerializedName("deg")
    @Expose
    var deg: Int? = null
)
package com.wakeel.weatheronimage.data.model

import com.google.gson.annotations.SerializedName

data class HistoryDto(
    @SerializedName("history") val results: MutableList<History>
)
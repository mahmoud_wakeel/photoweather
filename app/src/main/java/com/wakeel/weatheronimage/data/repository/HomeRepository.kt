package com.wakeel.weatheronimage.data.repository

import android.content.Context
import android.util.Log
import androidx.room.Room
import com.wakeel.weatheronimage.core.App
import com.wakeel.weatheronimage.data.db.HistoryDB
import com.wakeel.weatheronimage.data.model.History
import com.wakeel.weatheronimage.data.model.WeatherResponse
import com.wakeel.weatheronimage.data.network.WeatherWebServices
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeRepository(applicationContext: Context) {
    private var apiService: WeatherWebServices? = null
    val db = Room.databaseBuilder(
        applicationContext,
        HistoryDB::class.java, "history.db"
    ).build()

    init {
        apiService = App.api
    }

    fun getWeather(
        lat: Double,
        lon: Double,
        appId: String,
        callback: RepositoryCallback<WeatherResponse>
    ) {
        apiService!!.getWeatherByCity(lat, lon, appId).enqueue(object : Callback<WeatherResponse?> {
            override fun onFailure(call: Call<WeatherResponse?>?, t: Throwable?) {
                callback.onError(t!!.message.toString())
            }

            override fun onResponse(
                call: Call<WeatherResponse?>?,
                response: Response<WeatherResponse?>?
            ) {
                if (response != null) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onError(response.errorBody()!!.string())
                    }
                }
            }
        })

    }

    fun saveInDB(name: String, imagePath: String) {
        val history = History(name, imagePath)
        db.historyDao().insertImage(history)
    }

    interface RepositoryCallback<in T> {
        fun onSuccess(t: T?)
        fun onError(error: String)
    }

}
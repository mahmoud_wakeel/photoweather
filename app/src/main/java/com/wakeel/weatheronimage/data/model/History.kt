package com.wakeel.weatheronimage.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class History(
    @PrimaryKey
    var name: String = "",
    var path: String = ""
)
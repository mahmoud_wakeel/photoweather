package com.wakeel.weatheronimage.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.wakeel.weatheronimage.data.model.History

@Dao
interface HistoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertImage(image: History): Long

    @Query("SELECT * from History")
    fun selectAllImages(): List<History>
}
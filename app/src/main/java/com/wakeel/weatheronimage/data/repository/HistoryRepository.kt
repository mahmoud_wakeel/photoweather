package com.wakeel.weatheronimage.data.repository

import android.content.Context
import androidx.room.Room
import com.wakeel.weatheronimage.data.db.HistoryDB
import com.wakeel.weatheronimage.data.db.HistoryDao
import com.wakeel.weatheronimage.data.model.History
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class HistoryRepository(applicationContext: Context) {
    private val db = Room.databaseBuilder(
        applicationContext,
        HistoryDB::class.java, "history.db"
    ).build()

    fun getAllImages(): List<History>? {
        return db.historyDao().selectAllImages()
    }
}
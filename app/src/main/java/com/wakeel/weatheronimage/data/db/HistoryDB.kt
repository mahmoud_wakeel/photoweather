package com.wakeel.weatheronimage.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.wakeel.weatheronimage.core.App
import com.wakeel.weatheronimage.data.model.History


@Database(entities = [History::class], version = 1)
abstract class HistoryDB : RoomDatabase() {
    abstract fun historyDao(): HistoryDao

    companion object {
        @Volatile private var instance: HistoryDB? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
            HistoryDB::class.java, "todo-list.db")
            .build()
        /*const val DB_NAME = "weather.db"
        private val instance: HistoryDB by lazy { create(App.instance) }

        @Synchronized
        internal fun getInstance(): HistoryDB {
            return instance
        }

        private fun create(context: Context): HistoryDB {
            return Room.databaseBuilder(context, HistoryDB::class.java, DB_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }*/
    }
}
package com.wakeel.weatheronimage.data.network

import com.wakeel.weatheronimage.data.model.WeatherResponse
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.Call

interface WeatherWebServices {
    @GET("weather")
    fun getWeatherByCity(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("APPID") AppID: String?
    ): Call<WeatherResponse>
}
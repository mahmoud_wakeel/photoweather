package com.wakeel.weatheronimage.home

import com.wakeel.weatheronimage.data.model.WeatherResponse

interface HomeContract {
    interface View {
        fun showWait()
        fun removeWait()
        fun showWeather(weatherResponse: WeatherResponse)
        fun onFailure(message: String)
    }

    interface Presenter {
        fun getWeather(lat: Double, lon: Double, apiKey: String)
        fun saveImageToHistory(imagePath: String)
        fun attachView(view: View)
        fun detachView()
    }
}
package com.wakeel.weatheronimage.home

import com.wakeel.weatheronimage.data.model.WeatherResponse
import com.wakeel.weatheronimage.data.repository.HomeRepository


class HomePresenter(private val homeRepository: HomeRepository) : HomeContract.Presenter {
    var homeView: HomeContract.View? = null
    override fun getWeather(lat: Double, lon: Double, apiKey: String) {
        if (homeView != null) {
            homeView!!.showWait()
        }
        homeRepository.getWeather(
            lat,
            lon,
            apiKey,
            object : HomeRepository.RepositoryCallback<WeatherResponse> {
                override fun onSuccess(t: WeatherResponse?) {
                    if (homeView != null) {
                        homeView!!.removeWait()
                        homeView!!.showWeather(t!!)
                    }
                }

                override fun onError(error: String) {
                    if (homeView != null) {
                        homeView!!.removeWait()
                        homeView!!.onFailure(error)
                    }
                }
            })
    }

    override fun saveImageToHistory(imagePath: String) {
        var name = imagePath.replace(".jpg", "")
        val arr = name.split("_").toTypedArray()
        for (i in arr.indices) {
            if (i > 0)
                name = name + "_" + arr[i]
            else
                name = arr[i]
        }
        val filename: String = imagePath.substring(imagePath.lastIndexOf("/") + 1)

        homeRepository.saveInDB(filename, imagePath)
    }


    override fun attachView(view: HomeContract.View) {
        homeView = view
    }

    override fun detachView() {
        homeView = null
    }
}
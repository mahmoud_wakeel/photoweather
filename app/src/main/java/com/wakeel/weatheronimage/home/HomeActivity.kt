package com.wakeel.weatheronimage.home

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.squareup.picasso.Callback
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import com.wakeel.weatheronimage.R
import com.wakeel.weatheronimage.core.Config
import com.wakeel.weatheronimage.data.model.WeatherResponse
import com.wakeel.weatheronimage.data.repository.HomeRepository
import com.wakeel.weatheronimage.history.HistoryActivity
import com.wakeel.weatheronimage.utils.GPSTracker
import kotlinx.android.synthetic.main.activity_home.*
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class HomeActivity : AppCompatActivity(), HomeContract.View {
    var photoFile: File? = null
    private val captureImageRequest = 1
    private val locationRequestPermission = 100
    private val cameraRequestPermission = 200
    var mCurrentPhotoPath = ""
    private val imageDirectoryName = "WEATHER_ON_IMAGE"
    private var presenter: HomePresenter? = null
    private var gpsTracker: GPSTracker? = null
    private var space = 0
    private var imageBitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        removeWait()
        share_btn.setOnClickListener {
            val builder = StrictMode.VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "image/*"
            val imageUri = Uri.fromFile(File(mCurrentPhotoPath))
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
            startActivity(Intent.createChooser(shareIntent, "Share With.."))
        }
        history_btn.setOnClickListener {
            val intent = Intent(this@HomeActivity, HistoryActivity::class.java)
            startActivity(intent)
        }
        gpsTracker = GPSTracker(this)
        presenter = HomePresenter(HomeRepository(this))
        presenter!!.attachView(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            captureImage()
        } else {
            captureImage2()
        }
    }

    private fun captureImage2() {
        try {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            photoFile = createImageFile4()
            if (photoFile != null) {
                val photoURI: Uri = Uri.fromFile(photoFile)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(cameraIntent, captureImageRequest)
            }
        } catch (e: Exception) {
            displayMessage(baseContext, "Camera is not available.$e")
        }
    }

    private fun captureImage() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                cameraRequestPermission
            )
        } else {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(packageManager) != null) {
                try {
                    photoFile = createImageFile()
                    if (photoFile != null) {
                        val photoURI: Uri = FileProvider.getUriForFile(
                            this,
                            "com.wakeel.weatheronimage.fileprovider",
                            photoFile!!
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, captureImageRequest)
                    }
                } catch (ex: Exception) {
                    displayMessage(baseContext, ex.message.toString())
                }
            } else {
                displayMessage(baseContext, "Null")
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == captureImageRequest && resultCode == Activity.RESULT_OK) {
            imageBitmap = BitmapFactory.decodeFile(photoFile!!.absolutePath)
            val drawable = BitmapDrawable(resources, imageBitmap)
            image_captured.setImageDrawable(drawable)
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ),
                    locationRequestPermission
                )
            } else {
                if (gpsTracker!!.canGetLocation()) {
                    val latitude: Double = gpsTracker!!.latitude
                    val longitude: Double = gpsTracker!!.longitude
                    //call weather web service
                    presenter!!.getWeather(latitude, longitude, Config.APP_ID)
                } else {
                    Toast.makeText(this@HomeActivity, "Error", Toast.LENGTH_LONG).show()
                }
            }
            share_btn.visibility = VISIBLE
            history_btn.visibility = VISIBLE
        } else {
            displayMessage(baseContext, "Request cancelled or something went wrong.")
        }
    }

    private fun createImageFile4(): File? {
        val mediaStorageDir = File(
            Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            imageDirectoryName
        )
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                displayMessage(baseContext, "Unable to create directory.")
                return null
            }
        }
        val timeStamp: String = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.getDefault()
        ).format(Date())
        return File(
            mediaStorageDir.path + File.separator
                    + "IMG_" + timeStamp + ".jpg"
        )
    }

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,
            ".jpg",
            storageDir
        )
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    private fun displayMessage(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == cameraRequestPermission) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                grantResults[1] == PackageManager.PERMISSION_GRANTED
            ) {
                captureImage()
            }
        } else if (requestCode == locationRequestPermission) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (gpsTracker!!.canGetLocation()) {
                    val latitude: Double = gpsTracker!!.latitude
                    val longitude: Double = gpsTracker!!.longitude
                    //call weather web service
                    presenter!!.getWeather(latitude, longitude, Config.APP_ID)
                } else {
                    Toast.makeText(this@HomeActivity, "Error", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun showWait() {
        progressBar_home.visibility = VISIBLE
    }

    override fun removeWait() {
        progressBar_home.visibility = GONE
    }

    override fun showWeather(weatherResponse: WeatherResponse) {
        val text = weatherResponse.name + " _ " + weatherResponse.sys!!.country + "\\n" +
                weatherResponse.main!!.temp.toString() + "\\n" +
                weatherResponse.weather!![0].main!! + "\\n" +
                weatherResponse.main.humidity.toString()
        drawWeatherData(weatherResponse)
    }

    private fun drawWeatherData(weatherResponse: WeatherResponse) {
        val newBitmap: Bitmap = imageBitmap!!.copy(Bitmap.Config.ARGB_8888, true)
        var outputStream: FileOutputStream? = null
        val canvas = Canvas(newBitmap)
        val paint = Paint(Paint.FAKE_BOLD_TEXT_FLAG)
        paint.color = Color.BLACK
        paint.textSize = 160f

        for (text in getWeatherData(weatherResponse)) canvas.drawText(
            text,
            ((newBitmap.width / 2) - 700).toFloat(),
            (newBitmap.height / 2 + 220.let { space += it; space }).toFloat(),
            paint
        )


        val newModifiedCapturedImage = File(mCurrentPhotoPath)
        if (!newModifiedCapturedImage.exists()) {
            try {
                newModifiedCapturedImage.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        try {
            outputStream = FileOutputStream(newModifiedCapturedImage)
            newBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.flush()
                    outputStream.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                getImagePathAndSaveIt()
            }
        }
    }

    private fun getWeatherData(weatherResponse: WeatherResponse): MutableList<String> {
        val list: MutableList<String> =
            ArrayList()
        list.add(weatherResponse.name!! + "-" + weatherResponse.sys!!.country)
        list.add(weatherResponse.main!!.temp.toString())
        list.add(weatherResponse.weather!![0].main!!)
        list.add(weatherResponse.main.humidity.toString())
        return list
        /*  return mutableListOf(
              weatherResponse.name!! + "-" + weatherResponse.sys!!.country,
              weatherResponse.main!!.temp.toString(),
              weatherResponse.weather!![0].main!!,
              weatherResponse.main.humidity.toString()
          )*/
    }

    override fun onFailure(message: String) {
        Toast.makeText(this@HomeActivity, message, Toast.LENGTH_SHORT).show()
    }

    private fun getImagePathAndSaveIt() {
        if (mCurrentPhotoPath != "")
            Picasso.get().load(File(mCurrentPhotoPath)).memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(image_captured, object : Callback {
                    override fun onSuccess() {
                        share_btn.visibility = VISIBLE
                        Observable.fromCallable {
                            presenter!!.saveImageToHistory(mCurrentPhotoPath)
                        }.doOnNext { list ->

                        }.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe()
                    }

                    override fun onError(e: Exception) {
                        Toast.makeText(
                            this@HomeActivity,
                            "Something went wrong!!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                })
    }
}
